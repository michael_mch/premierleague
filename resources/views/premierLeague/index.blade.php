@php
    /** @var \App\Models\Scoreboards\Scoreboards\BaseScoreboard $scoreboard */
@endphp
@extends('premierLeague.layout')
@section('body')
    <div class="container">
        <h1>Premier League</h1>
        <div class="result-table">
            @include('premierLeague.' . $scoreboard->getScoreboardTemplateKey())
        </div>
        @include('premierLeague.' . $scoreboard->getScoreboardFormTemplateKey())
    </div>
@endsection
