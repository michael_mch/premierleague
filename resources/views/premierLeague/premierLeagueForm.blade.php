@php
    /** @var \App\Models\Scoreboards\Scoreboards\BaseScoreboard $scoreboard */
$i = 0;
@endphp
<div class="row">
    <div class="col-md-12">
        <h3>Match results</h3>
    </div>
</div>
<div id="form-errors"></div>
<form id="premier-league-form" action="{{ route('resultTable') }}" method="post">
    @foreach($scoreboard->getMatches() as $week => $matches)
        @if(count($matches))
            @if($i === 0 || $i === 3)
                <div class="row">
                    @endif
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">Week {{ $week }}</div>
                        </div>
                        @foreach($matches as $key => $match)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label
                                            for="{{ $week . '-' . $key . '-1' }}">{{ $match->getFirstTeamName() }}</label>
                                        <input type="text"
                                               class="form-control"
                                               name="results[{{ $week }}][{{ $key }}][0]"
                                               value="{{ $match->getResult()[0] }}"
                                               id="{{ $week . '-' . $key . '-1' }}"
                                               data-inputmask-integer="'min':1, 'max':100"
                                        >
                                        <small class="help-block"></small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label
                                            for="{{ $week . '-' . $key . '-2' }}">{{ $match->getSecondTeamName() }}</label>
                                        <input type="text"
                                               class="form-control"
                                               name="results[{{ $week }}][{{ $key }}][1]"
                                               value="{{ $match->getResult()[1] }}"
                                               id="{{ $week . '-' . $key . '-2' }}"
                                               data-inputmask-integer="'min':1, 'max':100"
                                        >
                                        <small class="help-block"></small>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @if($i === 2 || $i === 5)
                </div>
                <br><br>
            @endif
            @php
                $i ++;
            @endphp
        @endif
    @endforeach
</form>
