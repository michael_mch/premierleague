@php
    /** @var \App\Models\Scoreboards\Scoreboards\BaseScoreboard $scoreboard */
@endphp
<div class="row">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12"><h4>League Table</h4></div>
                        </div>
                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Teams</th>
                                    <th scope="col">Pts</th>
                                    <th scope="col">P</th>
                                    <th scope="col">W</th>
                                    <th scope="col">D</th>
                                    <th scope="col">L</th>
                                    <th scope="col">GD</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($scoreboard->getSortedTeams() as $team)
                                    <tr>
                                        <th scope="row">{{ $team->name }}</th>
                                        <td>{{ $team->points }}</td>
                                        <td>{{ $team->played }}</td>
                                        <td>{{ $team->won }}</td>
                                        <td>{{ $team->drawn }}</td>
                                        <td>{{ $team->lost }}</td>
                                        <td>{{ $team->goalsDifference }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12"><h4>Match Results</h4></div>
                        </div>
                        @if($scoreboard->getWeek())
                            <div class="row">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col" colspan="3">{{ $scoreboard->getWeek() }} <sup>th</sup> Week
                                            Match Result
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($scoreboard->getMatches()[$scoreboard->getWeek()] as $match)
                                        <tr>
                                            <td>{{ $match->getFirstTeamName() }}</td>
                                            <td>{{ $match->getResult()[0] }} - {{ $match->getResult()[1] }}</td>
                                            <td>{{ $match->getSecondTeamName() }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-primary play" data-week="{{ $scoreboard->getWeeksCount() }}">Play All
                        </button>
                    </div>
                    @if($scoreboard->getWeek() < $scoreboard->getWeeksCount())
                        <div class="col-md-6">
                            <button class="btn btn-primary play float-right"
                                    data-week="{{ $scoreboard->getWeek() + 1 }}">
                                Next Week
                            </button>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <table class="table">
            <thead>
            <tr>
                <th scope="col" colspan="2">{{ $scoreboard->getWeek() }} <sup>th</sup> Week Predictions of Championship
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($scoreboard->getSortedTeams() as $team)
                <tr>
                    <th scope="row">{{ $team->name }}</th>
                    <td>%{{ $team->chanceToWin }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<br><br>



