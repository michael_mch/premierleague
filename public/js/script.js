$(document).ready(function () {
    $('body').on('click', '.play', function (e) {
        e.preventDefault();
        $( '#form-errors' ).html('');
        let form = $('#premier-league-form');
        let data = form.serializeArray();
        data.push(
            {
                name: 'week',
                value: $(this).data('week')
            }
        );
        $.ajax({
            method : form.attr('method'),
            url : form.attr('action'),
            data : data,
            success : function (data) {
                $('.result-table').html(data);
            },
            error : function (data) {
                let errors = data.responseJSON;
                let errorsHtml = '<div class="alert alert-danger"><ul>';

                $.each(errors , function(key, value) {
                    errorsHtml += '<li>' + value[0] + '</li>';
                });
                errorsHtml += '</ul></di>';

                $( '#form-errors' ).html(errorsHtml);
            }

        });

        return false;
    })
});
