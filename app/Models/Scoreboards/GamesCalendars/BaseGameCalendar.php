<?php


namespace App\Models\Scoreboards\GamesCalendars;

use App\Models\Scoreboards\Matches\BaseMatch;
use App\Models\Scoreboards\ScoreboardException;
use App\Models\Scoreboards\Teams\BaseTeam;

/**
 * Class BaseGameCalendar
 *
 * @package App\Models\Scoreboards\GamesCalendars
 */
abstract class BaseGameCalendar
{
    protected $calendarConfig;
    /** @var BaseMatch[][]  */
    protected $matches = [];

    /**
     * BaseGameCalendar constructor.
     *
     * @throws ScoreboardException
     */
    public function __construct()
    {
        if (!file_exists($this->configFilePath)) {
            throw new ScoreboardException();
        }
        /** @noinspection PhpIncludeInspection */
        $this->calendarConfig = require $this->configFilePath;
    }

    /**
     * @param BaseTeam[] $teams
     */
    abstract function createMatches(array $teams): void;

    /**
     * @return BaseMatch[][]
     */
    public function getMatches(): array
    {
        return $this->matches;
    }
}
