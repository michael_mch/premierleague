<?php


namespace App\Models\Scoreboards\GamesCalendars;

use App\Models\Scoreboards\Matches\PremierLeagueMatch;

/**
 * Class PremierLeagueGamesCalendar
 *
 * @package App\Models\Scoreboards\GamesCalendars
 */
class PremierLeagueGamesCalendar extends BaseGameCalendar
{
    /** @var array */
    protected $configFilePath = __DIR__ . '/../data/calendarConfig.php';

    /**
     * @inheritDoc
     */
    public function createMatches(array $teams): void
    {
        $this->matches[0] = [];
        foreach ($this->calendarConfig as $week => $matches) {
            $this->matches[$week] = [];
            foreach ($matches as $match) {
                $newMatch = new PremierLeagueMatch();
                $newMatch->setFirstTeam($teams[$match['firstTeam']]);
                $newMatch->setSecondTeam($teams[$match['secondTeam']]);
                $newMatch->setResult($match['result']);

                $this->matches[$week][] = $newMatch;
            }
        }
    }
}
