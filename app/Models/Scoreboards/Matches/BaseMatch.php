<?php


namespace App\Models\Scoreboards\Matches;


use App\Models\Scoreboards\Teams\BaseTeam;

/**
 * Class BaseMatch
 *
 * @package App\Models\Scoreboards\Matches
 */
abstract class BaseMatch
{
    /** @var BaseTeam */
    protected $firstTeam;
    /** @var BaseTeam */
    protected $secondTeam;
    /** @var array */
    protected $result;

    /**
     * Calculates score and sets it to teams
     *
     * @return void
     */
    abstract public function calcTeamsScores(): void;

    /**
     * Calculates in percents chance to win for the first team
     *
     * @return int
     */
    abstract public function calcFirstTeamChance(): int;

    /**
     * Calculates in percents chance to win for the second team
     *
     * @return int
     */
    abstract public function calcSecondTeamChance(): int;

    /**
     * @return mixed
     */
    public function getFirstTeam()
    {
        return $this->firstTeam;
    }

    /**
     * @param mixed $firstTeam
     */
    public function setFirstTeam($firstTeam): void
    {
        $this->firstTeam = $firstTeam;
    }

    /**
     * @return mixed
     */
    public function getSecondTeam()
    {
        return $this->secondTeam;
    }

    /**
     * @param mixed $secondTeam
     */
    public function setSecondTeam($secondTeam): void
    {
        $this->secondTeam = $secondTeam;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result): void
    {
        $this->result = $result;
    }

    /**
     * @return string
     */
    abstract public function getFirstTeamName(): string;

    /**
     * @return string
     */
    abstract public function getSecondTeamName(): string;

}
