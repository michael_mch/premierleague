<?php


namespace App\Models\Scoreboards\Matches;

use App\Models\Scoreboards\Teams\PremierLeagueTeam;

/**
 * Class PremierLeagueMatch
 *
 * @package App\Models\Scoreboards\Matches
 *
 * @property PremierLeagueTeam $firstTeam
 * @property PremierLeagueTeam $secondTeam
 */
class PremierLeagueMatch extends BaseMatch
{
    const WON_POINTS = 3;
    const DRAWN_POINTS = 1;

    /**
     * @inheritDoc
     */
    public function calcTeamsScores(): void
    {
        $this->firstTeam->goalsDifference += $this->result[0] - $this->result[1];
        $this->secondTeam->goalsDifference += $this->result[1] - $this->result[0];
        $this->firstTeam->played ++;
        $this->secondTeam->played ++;
        if ($this->result[0] > $this->result[1]) {
            $this->firstTeam->won ++;
            $this->secondTeam->lost ++;
            $this->firstTeam->points += self::WON_POINTS;
        } elseif ($this->result[0] < $this->result[1]) {
            $this->firstTeam->lost ++;
            $this->secondTeam->won ++;
            $this->secondTeam->points += self::WON_POINTS;
        } else {
            $this->firstTeam->drawn ++;
            $this->firstTeam->points += self::DRAWN_POINTS;
            $this->secondTeam->drawn ++;
            $this->secondTeam->points += self::DRAWN_POINTS;
        }
    }

    /**
     * @inheritDoc
     */
    public function calcFirstTeamChance(): int
    {
        return intval($this->firstTeam->rating / ($this->firstTeam->rating + $this->secondTeam->rating) * 100);
    }

    /**
     * @inheritDoc
     */
    public function calcSecondTeamChance(): int
    {
        return intval($this->secondTeam->rating / ($this->firstTeam->rating + $this->secondTeam->rating) * 100);
    }

    /**
     * @return string
     */
    public function getFirstTeamName(): string
    {
        return $this->firstTeam->name;
    }

    /**
     * @return string
     */
    public function getSecondTeamName(): string
    {
        return $this->secondTeam->name;
    }
}
