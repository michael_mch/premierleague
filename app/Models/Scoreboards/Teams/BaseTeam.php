<?php


namespace App\Models\Scoreboards\Teams;

/**
 * Class BaseTeam
 *
 * @package App\Models\Scoreboards\Teams
 */
class BaseTeam
{
    /**
     * PremierLeagueTeam constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        foreach ($config as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }
}
