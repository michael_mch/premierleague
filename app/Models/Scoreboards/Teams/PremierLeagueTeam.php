<?php


namespace App\Models\Scoreboards\Teams;

/**
 * Class PremierLeagueTeam
 *
 * @package App\Models\Scoreboards
 */
class PremierLeagueTeam extends BaseTeam
{
    public $name = '';
    public $rating = 0;
    public $points = 0;
    public $played = 0;
    public $won = 0;
    public $drawn = 0;
    public $lost = 0;
    public $goalsDifference = 0;
    public $chanceToWin = 100;
}
