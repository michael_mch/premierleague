<?php

return [
    [
        'name' => 'Chelsea',
        'rating' => 80,
    ],
    [
        'name' => 'Arsenal',
        'rating' => 70,
    ],
    [
        'name' => 'Manchester City',
        'rating' => 60,
    ],
    [
        'name' => 'Liverpool',
        'rating' => 50,
    ],
];
