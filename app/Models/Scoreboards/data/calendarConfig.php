<?php

return [
    1 => [
        ['firstTeam' => 'Chelsea', 'secondTeam' => 'Manchester City', 'result' => [1, 2]],
        ['firstTeam' => 'Arsenal', 'secondTeam' => 'Liverpool', 'result' => [1, 3]],
    ],
    2 => [
        ['firstTeam' => 'Chelsea', 'secondTeam' => 'Liverpool', 'result' => [1, 2]],
        ['firstTeam' => 'Arsenal', 'secondTeam' => 'Manchester City', 'result' => [0, 1]],
    ],
    3 => [
        ['firstTeam' => 'Chelsea', 'secondTeam' => 'Arsenal', 'result' => [2, 1]],
        ['firstTeam' => 'Liverpool', 'secondTeam' => 'Manchester City', 'result' => [2, 0]],
    ],
    4 => [
        ['firstTeam' => 'Manchester City', 'secondTeam' => 'Chelsea', 'result' => [2, 2]],
        ['firstTeam' => 'Liverpool', 'secondTeam' => 'Arsenal', 'result' => [1, 3]],
    ],
    5 => [
        ['firstTeam' => 'Liverpool', 'secondTeam' => 'Chelsea', 'result' => [2, 3]],
        ['firstTeam' => 'Manchester City', 'secondTeam' => 'Arsenal', 'result' => [4, 1]],
    ],
    6 => [
        ['firstTeam' => 'Arsenal', 'secondTeam' => 'Chelsea', 'result' => [2, 0]],
        ['firstTeam' => 'Manchester City', 'secondTeam' => 'Liverpool', 'result' => [0, 3]],
    ],
];
