<?php


namespace App\Models\Scoreboards;

use App\Models\Scoreboards\Scoreboards\BaseScoreboard;
use App\Models\Scoreboards\Scoreboards\PremierLeagueScoreboard;

/**
 * Class ScoreboardFactory
 *
 * @package App\Models\Scoreboards
 */
class ScoreboardFactory
{

    /**
     * @return BaseScoreboard
     * @throws ScoreboardException
     */
    public function createScoreboard(): BaseScoreboard
    {
        return new PremierLeagueScoreboard();
    }
}
