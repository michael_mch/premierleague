<?php


namespace App\Models\Scoreboards\Scoreboards;


use App\Models\Scoreboards\GamesCalendars\BaseGameCalendar;
use App\Models\Scoreboards\GamesCalendars\PremierLeagueGamesCalendar;
use App\Models\Scoreboards\Matches\BaseMatch;
use App\Models\Scoreboards\Matches\PremierLeagueMatch;
use App\Models\Scoreboards\ScoreboardException;
use App\Models\Scoreboards\Teams\PremierLeagueTeam;

/**
 * Class PremierLeagueScoreboard
 *
 * @package App\Models\Scoreboards\Tournaments
 */
class PremierLeagueScoreboard extends BaseScoreboard
{
    /** @var BaseGameCalendar */
    private $gamesCalendar;
    /** @var array */
    private $teamsConfig;
    private $configFilePath = __DIR__ . '/../data/teamsConfig.php';
    /** @var PremierLeagueTeam[] */
    private $teams = [];
    private $gamesResults;
    private $week;

    /**
     * PremierLeagueScoreboard constructor.
     *
     * @throws ScoreboardException
     */
    public function __construct()
    {
        if (!file_exists($this->configFilePath)) {
            throw new ScoreboardException();
        }
        /** @noinspection PhpIncludeInspection */
        $this->teamsConfig = require $this->configFilePath;
        $this->createTeams();
        $this->createGamesCalendar();
        $this->gamesCalendar->createMatches($this->teams);
    }

    /**
     * @inheritDoc
     */
    public function calcScores(): void
    {
        $matches = $this->gamesCalendar->getMatches();
        for ($i = 0; $i <= $this->week; $i++) {
            /** @var BaseMatch $match */
            foreach ($matches[$i] as $match) {
                $match->calcTeamsScores();
            }
        }

    }

    /**
     * @inheritDoc
     */
    public function calcTeamsChances(): void
    {
        $totalWeeks = count($this->gamesCalendar->getMatches()) - 1;
        $totalRating = 0;
        $maxPoints = 0;
        foreach ($this->teams as $team) {
            $maxPoints = max($maxPoints, $team->points);
            $totalRating += $team->rating;
        }
        foreach ($this->teams as $team) {
            $team->chanceToWin = round($team->rating / $totalRating * 100);
            $possiblePoints = ($totalWeeks - $team->played) * PremierLeagueMatch::WON_POINTS;

            $pointsDiff = $maxPoints - $team->points;
            if ($possiblePoints < $pointsDiff) {
                $team->chanceToWin = 0;
            }

            if ($pointsDiff === 0) {
                $team->chanceToWin += 60;
            } else {
                $team->chanceToWin += $possiblePoints / $pointsDiff * 10;
            }
        }

        $totalChance = 0;
        foreach ($this->teams as $team) {
            $totalChance += $team->chanceToWin;
        }

        foreach ($this->teams as $team) {
            $team->chanceToWin = round($team->chanceToWin / $totalChance * 100, 2);
        }
    }

    private function createGamesCalendar()
    {
        $this->gamesCalendar = new PremierLeagueGamesCalendar();
    }

    /**
     * Creates teams based on config.
     */
    private function createTeams()
    {
        if (empty($this->teams)) {
            foreach ($this->teamsConfig as $item) {
                $team = new PremierLeagueTeam($item);
                $this->teams[$team->name] = $team;
            }
        }
    }

    /**
     * @param array $gamesResults
     */
    public function setGamesResults(array $gamesResults): void
    {
        $this->gamesResults = $gamesResults;

        $matches = $this->gamesCalendar->getMatches();
        foreach ($gamesResults as $week => $results) {
            foreach ($results as $key => $result) {
                /** @var BaseMatch $match */
                $match = $matches[$week][$key];
                $match->setResult($result);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function getScoreboardTemplateKey(): string
    {
        return 'premierLeagueScoreboard';
    }

    /**
     * @inheritDoc
     */
    public function getScoreboardFormTemplateKey(): string
    {
        return 'premierLeagueForm';
    }

    public function getWeeksCount(): int
    {
        return count($this->gamesCalendar->getMatches()) - 1;
    }

    /**
     * @inheritDoc
     */
    public function getMatches(): array
    {
        return $this->gamesCalendar->getMatches();
    }

    /**
     * @inheritDoc
     */
    public function getSortedTeams(): array
    {
        $teams = $this->teams;

        usort($teams, function ($a, $b) {
            if ($a->points == $b->points) {
                return 0;
            }
            return ($a->points < $b->points) ? 1 : -1;
        });

        return $teams;
    }

    /**
     * @param int $week
     */
    public function setWeek(int $week): void
    {
        $this->week = $week;
    }

    /**
     * @return int
     */
    public function getWeek(): int
    {
        return $this->week;
    }
}
