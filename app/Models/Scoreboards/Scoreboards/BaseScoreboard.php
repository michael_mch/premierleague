<?php


namespace App\Models\Scoreboards\Scoreboards;

/**
 * Class BaseScoreboard
 *
 * @package App\Models\Scoreboards\Tournaments
 */
abstract class BaseScoreboard
{
    abstract public function getWeeksCount(): int;
    /**
     * @param array $results
     */
    abstract public function setGamesResults(array $results): void;

    /**
     * Calculates scores based on tournament round and games results.
     */
    abstract public function calcScores(): void;

    /**
     * Calculates teams chances to win
     */
    abstract public function calcTeamsChances(): void;

    /**
     * @return string
     */
    abstract public function getScoreboardTemplateKey(): string;

    /**
     * @return string
     */
    abstract public function getScoreboardFormTemplateKey(): string;

    /**
     * @return array
     */
    abstract public function getMatches(): array;

    /**
     * @return array
     */
    abstract public function getSortedTeams(): array;

    /**
     * @param int $week
     */
    abstract public function setWeek(int $week): void;

    /**
     * @return int
     */
    abstract public function getWeek(): int;
}
