<?php

namespace App\Http\Controllers;


use App\Models\Scoreboards\ScoreboardException;
use App\Models\Scoreboards\ScoreboardFactory;
use Illuminate\Http\Request;

class PremierLeagueController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $scoreboardFactory = new ScoreboardFactory();
        try {
            $scoreboard = $scoreboardFactory->createScoreboard();
        } catch (ScoreboardException $e) {
            abort(500, 'Invalid configuration.');
        }
        $scoreboard->setWeek(0);
        $scoreboard->calcScores();
        $scoreboard->calcTeamsChances();

        return view('premierLeague.index', ['scoreboard' => $scoreboard]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getResultTable(Request $request)
    {
        $scoreboardFactory = new ScoreboardFactory();
        try {
            $scoreboard = $scoreboardFactory->createScoreboard();
        } catch (ScoreboardException $e) {
            abort(500, 'Invalid configuration.');
        }
        $this->validate($request, [
            'week' => 'required|integer|min:0|max:' . $scoreboard->getWeeksCount(),
            'results.*.*.*' => 'required|integer|min:0|max:100'
        ]);

        $scoreboard->setGamesResults($request->get('results'));
        $scoreboard->setWeek($request->get('week'));
        $scoreboard->calcScores();
        $scoreboard->calcTeamsChances();

        return view('premierLeague.' . $scoreboard->getScoreboardTemplateKey(), ['scoreboard' => $scoreboard]);
    }
}
