<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', [
    'as' => 'index', 'uses' => 'PremierLeagueController@index'
]);

$router->post('/result-table', [
    'as' => 'resultTable', 'uses' => 'PremierLeagueController@getResultTable'
]);

